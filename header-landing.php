<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <?php // force Internet Explorer to use the latest rendering engine available ?>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <?php // mobile meta (hooray!) ?>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>

  <?php // icons & favicons ?>
  <?php // Using REAL FAVICON GENERATOR? Put generated code in file below + uncomment admin favicon setup (in admin.php) ?>
  <?php // get_template_part( 'favicon-head' ); ?>

  <?php // other html head stuff (before WP/theme scripts are loaded) ?>
  <script src="https://use.fontawesome.com/ad04f5a461.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Roboto+Slab|Roboto:300,400,400i,700,900" rel="stylesheet">

  <!-- wordpress head functions -->
  <?php wp_head(); ?>
  <!-- end of wordpress head -->

  <?php // drop Google Analytics here ?>
  <?php // end Analytics ?>

</head>

<body <?php body_class(pretty_body_class()); ?> itemscope itemtype="http://schema.org/WebPage">
  <!--[if lt IE 11]><p class="browserwarning">Your browser is <strong>very old.</strong> Please <a href="http://browsehappy.com/">upgrade to a different browser</a> to properly browse this web site.</p><![endif]-->
