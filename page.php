<?php get_header(); ?>



<?php if (get_field('banner_image')): ?>
  <div class="Strip TitleBanner" style="background-image:url(  <?php echo get_field('banner_image'); ?>)">
<?php else: ?>
  <div class="Strip TitleBanner" style="background-image:url(<?php echo get_field('defualt_banner_image', 'option'); ?> )">
<?php endif; ?>

  <div class="SectionContainer u-responsivePadding" style="position:relative; height:100%;">
      <h1 class="MainTitle u-verticalCenterTransform"><?php the_title(); ?></h1>
  </div>
</div>

  <div class="Strip">
    <main class="SectionContainerFull" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/WebPageElement">

      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <section class="EntryContent  cf">
          <?php the_content(); ?>
        </section> <!-- /EntryContent -->
      <?php endwhile; endif; // END main loop (if/while) ?>


    </main>
  </div> <!-- /Strip-->

  <?php if (is_page(128)): ?>
    <?php get_template_part( 'parts/booknow' ); ?>
  <?php endif; ?>

<?php get_footer(); ?>
