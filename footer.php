
<!-- Adds Review Post Type Slide to bottom of Footer -->
<?php if (!is_page(54)) { ?>


<div class="Strip ReviewBanner">
  <div class="SectionContainer u-relative">
    <div class="Reviews  ">
      <?php
        $args = array(
          'posts_per_page' => 5,
          'post_type' => 'review_type',
          'orderby' => 'rand'
          // 'order' => 'asc'
        );
        $cpt_query = new WP_Query($args);
      ?>
        <div class="ReviewSlider">
      <?php if ($cpt_query->have_posts()) : while ($cpt_query->have_posts()) : $cpt_query->the_post(); ?>


          <div class="ReviewSlider-item">
          <?php echo the_content(); ?><br /><span class="author"><?php echo get_field('review_author'); ?></span>
          </div>


      <?php endwhile; endif; // end of CPT loop ?>
        </div> <!-- /ReviewSlider -->
      <?php wp_reset_postdata(); ?>

    </div> <!-- /Reviews -->
  </div> <!-- /SectionContainer -->
</div>

<?php } ?>
<!-- END Adds Review Post Type Slide to bottom of Footer -->

<footer class="Strip u-responsivePadding PageFooter" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
  <div class="SectionContainer FooterColumns">

    <div class="FooterColumn">

        <?php if ( is_active_sidebar( 'footer-one' ) ) : ?>
        <?php dynamic_sidebar( 'footer-one' ); ?>
      <?php endif; ?>

    </div>

    <div class="FooterColumn">
      <?php if ( is_active_sidebar( 'footer-two' ) ) : ?>
          <?php dynamic_sidebar( 'footer-two' ); ?>
        <?php endif; ?>
    </div>

      <div class="FooterColumn">
        <?php if ( is_active_sidebar( 'footer-third' ) ) : ?>
        <?php dynamic_sidebar( 'footer-third' ); ?>
      <?php endif; ?>
      </div>

      <div class="FooterColumn">
        <?php if ( is_active_sidebar( 'footer-forth' ) ) : ?>
        <?php dynamic_sidebar( 'footer-forth' ); ?>
      <?php endif; ?>
      </div>




  </div> <!-- /container-->
  <div class="FooterLower">
  <span class="SiteCopyright">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?>.</span>
  </div>
</footer> <!-- /footer -->

<!-- Navigation -->
<div id="ModalNav" class="ModalNav">
  <div class="close-ModalNav  CloseModalNavButton"> X <!-- class name must match id above, ie. close-IdName -->
  </div>

  <div class="ModalNavWrap">
    <img src="" alt="" />
    <nav class="MobileMenuWrap" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
      <!-- Use this or list pages version -->
      <ul class="MobileMenu">
    <?php
      $pageargs=array(
        'depth' => 2,
        'exclude' => '',
        'title_li' => '',
        'sort_column'=> 'menu_order',
        'sort_order'=> 'asc',
        'walker'=> new FDT_Arrow_Walker_List_Pages()
        );
      wp_list_pages($pageargs);
    ?>
  </ul>
    </nav>
  </div> <!-- /ModalNavWrap -->
</div> <!-- /ModalNav -->

<!-- all js scripts are loaded in lib/fdt-enqueues.php -->
<?php wp_footer(); ?>

</div> <!-- for mmenu -->
</body>

</html> <!-- end page. what a ride! -->
