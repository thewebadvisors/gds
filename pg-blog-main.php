<?php
/*
Template Name: Blog Main Page
*/
?>

<?php get_header(); ?>


  <div class="Strip TitleBanner" style="background-image:url(<?php echo get_field('defualt_banner_image', 'option'); ?> )">
    <div class="SectionContainer" style="position:relative; height:100%;">
        <h1 class="MainTitle u-verticalCenterTransform"><?php the_title(); ?></h1>
    </div>
  </div>

<div class="Strip ">
  <main class="SectionContainer u-responsivePadding" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/WebPageElement">
    <div class="PrimaryContent">

      <h1 class="MainTitle">Blog</h1>

      <?php
      $temp = $wp_query;
      $wp_query = null;
      $wp_query = new WP_Query(
        array(
          'posts_per_page' => 4,
          'paged' => $paged
        )
      );
      ?>

      <?php if ($wp_query->have_posts()) : while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

        <article <?php post_class('BlogWrap'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

          <div class="BlogWrap-image">
            <?php the_post_thumbnail('medium'); ?>
          </div>

          <div class="BlogWrap-content">



            <h2 itemprop="headline"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>

            <div class="EntryMeta">
              <span>Date: <time datetime="<?php the_time('Y-m-d'); ?>" itemprop="datePublished"><?php the_time('d.m.Y'); ?></time></span>
              <span><?php the_category(' / '); ?></span>
            </div> <!-- /EntryMeta -->

            <section class="EntryContent  BlogContent  cf" itemprop="articleBody">

              <?php the_excerpt(); ?>

              <!--
              !!!!!! DELETE ME IF NOT USING !!!!!!!
              <?php global $more; $more = 0; ?>
              <?php the_content("Read More..."); ?>
            -->



          </section> <!-- /EntryContent -->

            </div>

        </article> <!-- /article -->



    <?php endwhile; endif; ?>

    <?php /* Display navigation to next/previous pages when applicable */ ?>
    <?php if ( $wp_query->max_num_pages > 1 ) : ?>
      <?php $max_page = $wp_query->max_num_pages; ?>
      <nav class="PostNav">
        <ul class="cf">
          <li class="PostNav-prev"><?php next_posts_link(__('&laquo; Older Entries', 'flexdev')) ?></li>
          <?php if (($paged < $max_page) && ($paged > 1))  { echo "<li class='nav-divider'><span>|</span></li>"; }  ?>
          <li class="PostNav-next"><?php previous_posts_link(__('Newer Entries &raquo;', 'flexdev')) ?></li>
        </ul>
      </nav>
    <?php endif; ?>

    <?php $wp_query = null; $wp_query = $temp; ?>
    <?php wp_reset_postdata(); ?>

  </div> <!-- /PrimaryContent -->

  <?php get_sidebar(); // sidebar ?>

</main>
</div> <!-- /Strip -->

<?php get_footer(); ?>
