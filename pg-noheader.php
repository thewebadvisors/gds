<?php
/*
Template Name: No Header Page
*/
?>

<?php get_header(); ?>

  <div class="Strip">
    <main class="SectionContainer" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/WebPageElement">

      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <section class="EntryContent  cf">
          <?php the_content(); ?>
        </section> <!-- /EntryContent -->
      <?php endwhile; endif; // END main loop (if/while) ?>

      <?php if (is_page(14)): ?>
        <?php get_template_part( 'parts/booknow' ); ?>
      <?php endif; ?>

    </main>
  </div> <!-- /Strip-->

<?php get_footer(); ?>
