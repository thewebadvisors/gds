<?php get_header(); ?>

<div class="Strip">


  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>


    <div class="PrimaryContent">

      <article <?php post_class('cf'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

        <header class="ArticleHeader" style="background-image: url(<?php if (current_theme_supports( 'post-thumbnails' ) && has_post_thumbnail( $post->ID )) {
          $page_bg_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
          $page_bg_image_url = $page_bg_image[0]; // this returns just the URL of the image
          echo $page_bg_image_url;
        } ?> );">

        <div class="SectionContainer">
          <h1 itemprop="headline  u-verticalCenter"><?php the_title(); ?></h1>
        </div>

      </header> <!-- /ArticleHeader -->

      <main class="SectionContainer" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/WebPageElement">

        <section class="EntryContent  BlogContent  cf" itemprop="articleBody">
          <div class="EntryMeta">
            <span><time datetime="<?php the_time('Y-m-d'); ?>" itemprop="datePublished"><?php the_time('M d, Y'); ?></time></span>
            <!-- <span>Categories: <?php the_category(', '); ?></span> -->
          </div> <!-- /EntryMeta -->
          <?php the_content(); ?>

        </section> <!-- /EntryContent -->

        <footer class="ArticleFooter">
          <div class="LinkButton">
            <a href="/blog">&laquo; All Posts</a>
          </div>
        </footer> <!-- /article footer -->

      </article> <!-- /article -->

    <?php endwhile; else : ?>

      <article class="PostNotFound">
        <header class="ArticleHeader">
          <h1><?php _e("Oops, Post Not Found!", "flexdev"); ?></h1>
        </header>
        <section class="EntryContent">
          <p><?php _e("Uh Oh. Something is missing. Try double checking things.", "flexdev"); ?></p>
        </section>
        <footer class="ArticleFooter">
          <p><?php _e("This is the error message in the single.php template.", "flexdev"); ?></p>
        </footer>
      </article>

    <?php endif; ?>

  </div> <!-- /PrimaryContent -->

</main>
</div> <!-- /Strip-->

<?php get_footer(); ?>
