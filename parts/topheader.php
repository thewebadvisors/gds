<div class="Strip  Strip--nopad  Fixed  TopHeader">
  <div class="SectionContainer">
    <div class="HeaderUpper">
      <span><?php if( get_field('phone_number', 'options') ) {
        echo  get_field('phone_number', 'options') ;
      }?></span>|
      <a class="MailLink  popup-with-form" href="#HeaderContactForm"> <i class="fa fa-envelope"></i></a>


      <div class="HeaderCTAs"><a href="/golf/book-online">Tee Times <i class="fa fa-clock-o" aria-hidden="true"></i></a></div>
      <div class="HeaderSocial">
        <a class="hs-facebook" target="_blank" href="#"><i title="facebook" class="fa fa-facebook" aria-hidden="true"></i><span class="u-hidden">Facebook</span></a><a target="_blank" class="hs-tripadvisor" href=""><i title="hs-tripadvisor" class="fa fa-tripadvisor" aria-hidden="true"></i><span class="u-hidden">Tripadvisor</span></a><a class="hs-google" target="_blank" href=""><i title="Goolge" class="fa fa-google" aria-hidden="true"></i><span class="u-hidden">Google Plus</span></a>
      </div>

    </div>
  </div>
</div>
