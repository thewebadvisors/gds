<div id="ModalNav" class="ModalNav">
  <div class="close-ModalNav  CloseModalNavButton"> <!-- class name must match id above, ie. close-IdName -->
    CLOSE BUTTON
  </div>

  <div class="ModalNavWrap">
    <h2>MENU</h2>
    <nav class="MobileMenuWrap" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">

      <ul class="MobileMenu">
        <?php
          $pageargs=array(
            'depth' => 2,
            'exclude' => '',
            'title_li' => '',
            'sort_column'=> 'menu_order',
            'sort_order'=> 'asc',
            );
          wp_list_pages($pageargs);
        ?>
      </ul>

      <?php // fdt_nav_menu( 'main-nav', 'menu-mobile' ); // Adjust using Menus in WordPress Admin ?>

    </nav>
  </div> <!-- /ModalNavWrap -->
</div> <!-- /ModalNav -->
