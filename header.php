<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <?php // force Internet Explorer to use the latest rendering engine available ?>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <?php // mobile meta (hooray!) ?>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <meta name="msvalidate.01" content="C582D07B7CCA60AAE2E4989756254AE2" />
  <?php // icons & favicons ?>
  <?php // Using REAL FAVICON GENERATOR? Put generated code in file below + uncomment admin favicon setup (in admin.php) ?>
  <?php // get_template_part( 'favicon-head' ); ?>

  <?php // other html head stuff (before WP/theme scripts are loaded) ?>
  <script src="https://use.fontawesome.com/ad04f5a461.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700|Raleway:400,400i,600,700" rel="stylesheet">

  <!-- wordpress head functions -->
  <?php wp_head(); ?>
  <!-- end of wordpress head -->

  <?php // drop Google Analytics here ?>
  <?php // end Analytics ?>

</head>

<body <?php body_class(pretty_body_class()); ?> itemscope itemtype="http://schema.org/WebPage">
  <!--[if lt IE 11]><p class="browserwarning">Your browser is <strong>very old.</strong> Please <a href="http://browsehappy.com/">upgrade to a different browser</a> to properly browse this web site.</p><![endif]-->


  <!-- for mmenu --><div>

  <div class="Strip TopHeader">
    <div class="SectionContainer">
      <div class="SiteLogo" itemscope itemtype="http://schema.org/Organization">
        <a href="/" rel="nofollow">
          <img src="<?php bloginfo('template_url') ?>/assets/img/Logo.png" alt="<?php bloginfo('name'); ?>" />
          <div class="LogoTitle">
            <p class="LogoHeader">G.Doran &amp; Sons Ltd.<br /><span>Plumbing and Heating</span></p>
          </div>
        </a>
      </div>
      <div class="HeaderContact">
        <p><span itemprop="telephone">24/7 : <a href="tel:2507435493">(250) 743-5493</a></span></p>

      </div>
    </div>
  </div>

  <header class="Strip  Strip--nopad  PageHeader " role="banner" itemscope itemtype="http://schema.org/WPHeader">

    <div class="SectionContainer">

      <nav class="MainMenuWrap" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
        <ul class="MainMenu">

          <?php
          $pageargs=array(
            'depth' => 2,
            'title_li' => '',
            'sort_column' => 'menu_order',
            'sort_order' => 'asc',
            'include_tree' => '66,46,47,49,50,51'
          );
          ub_wp_list_pages($pageargs);
          ?>

          <!-- <li class="MenuLinkButton"><a href="/blog">Blog</a></li> -->
        </ul>
        <?php // fdt_nav_menu( 'main-nav', 'MainMenu' ); // Adjust using Menus in WordPress Admin ?>
      </nav>

      <nav class="TertiaryMenu" role="navigation">
        <?php fdt_nav_menu( 'tertiary-nav', 'TertiaryMenu' ); // Adjust using Menus in WordPress Admin -- REMOVE if not using ?>
      </nav>


      <nav role="navigation" id="nav-menu" class="nav-menu">
        <ul>
          <?php
          $pageargs=array(
            'depth' => 2,
            'title_li' => '',
            'sort_column' => 'menu_order',
            'sort_order' => 'asc',
            'include_tree' => '6,9,101,110,26,10'
          );
          ub_wp_list_pages($pageargs);
          ?>
        </ul>
      </nav>

      <a href="#ModalNav" id="ModalMenuButton" class="ModalMenuButton">
        <p>Menu</p>
        <span class="top"></span>
        <span class="middle"></span>
        <span class="bottom"></span>
      </a>


    </div> <!-- /SectionContainer -->

  </header> <!-- /PageHeader -->
