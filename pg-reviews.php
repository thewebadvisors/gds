<?php
/*
Template Name: Reviews Page
*/
?>

<?php get_header(); ?>

<?php if (get_field('banner_image')): ?>
  <div class="Strip TitleBanner" style="background-image:url(  <?php echo get_field('banner_image'); ?>)">
<?php else: ?>
  <div class="Strip TitleBanner" style="background-image:url(<?php echo get_field('defualt_banner_image', 'option'); ?> )">
<?php endif; ?>

  <div class="SectionContainer  u-responsivePadding" style="position:relative; height:100%;">
      <h1 class="MainTitle u-verticalCenterTransform"><?php the_title(); ?></h1>
  </div>
</div>





  <div class="Strip Strip--afterheader">
    <main class="SectionContainer  u-responsivePadding" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/WebPageElement">



        <section class="EntryContent  cf">

          <?php echo get_field('review_content'); ?>


          <div class="ReviewGroup">
            <?php
            $args = array(
              'posts_per_page' => -1,
              'post_type' => 'review_type',
              'orderby' => 'rand'
              // 'order' => 'asc'
            );
            $cpt_query = new WP_Query($args);
            ?>
            <?php if ($cpt_query->have_posts()) : while ($cpt_query->have_posts()) : $cpt_query->the_post(); ?>
              <?php
              // vars
              $ratingfield = get_field_object('review_rating');
              $value = $ratingfield['value'];
              $label = $ratingfield['choices'][ $value ];
              ?>
              <div class="ReviewItem" itemscope itemtype="http://schema.org/Review">
                <div itemprop="itemReviewed" itemscope itemtype="http://schema.org/LocalBusiness">
                  <meta itemprop="image" content="<?php bloginfo('template_url') ?>/assets/img/logo.jpg" />
                  <meta itemprop="name" content="<?php bloginfo('name'); ?>" />


                </div> <!-- /CLASS-NAME-HERE -->
                <?php if( get_field('review_rating') ) {
                  echo ' <span class="ReviewRating-image '.$value.'"></span>';
                }?>

                <div itemprop="reviewBody" class="ReviewContent"><?php the_content(); ?></div>
                <div itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating"  />
                  <meta itemprop="ratingValue" content="<?php echo $label; ?>" >
                </div>
                <?php if( get_field('review_author') ) {
                  echo '<span itemprop="author" itemscope itemtype="http://schema.org/Person"><p class="ReviewItem-author"><span itemprop="name">' . get_field('review_author') . '</span></p></span>';
                }?>

                <?php if( get_field('review_source') ) {
                  echo '<span class="ReviewItem-source"> Review Source: ' . get_field('review_source') . '</span>';
                }?>


            </div>
            <?php endwhile; endif; // end of CPT loop ?>
          </div>

          <?php wp_reset_postdata(); ?>




        </section> <!-- /EntryContent -->



      <?php // IF USING PARTS ->  get_template_part( 'parts/name-of-part' ); ?>

    </main>
  </div> <!-- /Strip-->

  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  <div class="Strip">
    <div class="SectionContainerFull">

      <?php the_content(); ?>

    </div> <!-- /SectionContainer -->
  </div>
    <?php endwhile; endif; // END main loop (if/while) ?>

<?php get_footer(); ?>
